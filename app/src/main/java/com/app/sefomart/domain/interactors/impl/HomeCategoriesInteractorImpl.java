package com.app.sefomart.domain.interactors.impl;

import android.util.Log;

import com.app.sefomart.Network.ApiClient;
import com.app.sefomart.Network.response.CategoryResponse;
import com.app.sefomart.Network.services.HomeCategoryApiInterface;
import com.app.sefomart.domain.executor.Executor;
import com.app.sefomart.domain.executor.MainThread;
import com.app.sefomart.domain.interactors.HomeCategoriesInteractor;
import com.app.sefomart.domain.interactors.base.AbstractInteractor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeCategoriesInteractorImpl extends AbstractInteractor {
    private HomeCategoriesInteractor.CallBack mCallback;
    private HomeCategoryApiInterface apiService;

    public HomeCategoriesInteractorImpl(Executor threadExecutor, MainThread mainThread, HomeCategoriesInteractor.CallBack callBack) {
        super(threadExecutor, mainThread);
        mCallback = callBack;
    }

    @Override
    public void run() {
        apiService = ApiClient.getClient().create(HomeCategoryApiInterface.class);
        Call<CategoryResponse> call = apiService.getHomeCategories();

        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                try {
                    mCallback.onHomeCategoriesDownloaded(response.body().getData());
                } catch (Exception e) {
                    Log.e("Exception", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                mCallback.onHomeCategoriesDownloadError();
            }
        });

    }
}
