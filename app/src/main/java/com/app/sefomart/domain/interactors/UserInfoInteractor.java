package com.app.sefomart.domain.interactors;

import com.app.sefomart.Models.User;

public interface UserInfoInteractor {
    interface CallBack {

        void onUserInfoLodaded(User user);

        void onUserInfoError();
    }
}
