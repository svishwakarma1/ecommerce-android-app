package com.app.sefomart.domain.interactors;

import com.app.sefomart.Network.response.ShippingInfoUpdateResponse;

public interface ShippingInfoUpdateInteractor {
    interface CallBack {

        void onShippingInfoUpdated(ShippingInfoUpdateResponse shippingInfoUpdateResponse);

        void onShippingInfoUpdatedError();
    }
}
