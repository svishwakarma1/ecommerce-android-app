package com.app.sefomart.domain.interactors;

import com.app.sefomart.Models.Shop;

public interface ShopInteractor {
    interface CallBack {

        void onShopLoaded(Shop shop);

        void onShopLoadError();
    }
}
