package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Presentation.ui.activities.impl.PaymentActivity;

public interface PaymentSelectListener {
    void onPaymentItemClick(PaymentActivity.PaymentModel paymentModel);
}
