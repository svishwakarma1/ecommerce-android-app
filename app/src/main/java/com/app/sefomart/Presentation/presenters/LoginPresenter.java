package com.app.sefomart.Presentation.presenters;

import com.app.sefomart.Network.response.AuthResponse;
import com.app.sefomart.Presentation.ui.activities.LoginView;
import com.app.sefomart.domain.executor.Executor;
import com.app.sefomart.domain.executor.MainThread;
import com.app.sefomart.domain.interactors.LoginInteractor;
import com.app.sefomart.domain.interactors.impl.LoginInteractorImpl;

public class LoginPresenter extends AbstractPresenter implements LoginInteractor.CallBack {

    private LoginView loginView;

    public LoginPresenter(Executor executor, MainThread mainThread, LoginView loginView) {
        super(executor, mainThread);
        this.loginView = loginView;
    }

    public void validLogin(String email, String password) {
        new LoginInteractorImpl(mExecutor, mMainThread, this, email, password).execute();
    }

    @Override
    public void onValidLogin(AuthResponse authResponse) {
        if (loginView != null){
            loginView.setLoginResponse(authResponse);
        }
    }

    @Override
    public void onLoginError() {

    }
}
