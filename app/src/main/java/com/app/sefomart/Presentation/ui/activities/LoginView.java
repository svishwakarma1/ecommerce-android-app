package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Network.response.AuthResponse;

public interface LoginView {
    public void setLoginResponse(AuthResponse authResponse);
}
