package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Models.WishlistProduct;

public interface WishlistProductClickListener {
    void onProductItemClick(WishlistProduct product);
}
