package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Models.SubSubCategory;

public interface SubSubCategoryClickListener {
    void onSubSubCategoryClick(SubSubCategory subSubCategory);
}
