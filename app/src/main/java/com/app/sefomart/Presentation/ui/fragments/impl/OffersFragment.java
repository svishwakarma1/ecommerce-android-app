package com.app.sefomart.Presentation.ui.fragments.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.sefomart.Models.FlashDeal;
import com.app.sefomart.Models.Product;
import com.app.sefomart.Network.response.FlashDealResponse;
import com.app.sefomart.Network.response.ProductListingResponse;
import com.app.sefomart.Presentation.presenters.OfferPresenter;
import com.app.sefomart.Presentation.presenters.ProductListingPresenter;
import com.app.sefomart.Presentation.ui.activities.impl.ProductDetailsActivity;
import com.app.sefomart.Presentation.ui.activities.impl.ProductListingActivity;
import com.app.sefomart.Presentation.ui.adapters.ProductListingAdapter;
import com.app.sefomart.Presentation.ui.fragments.OfferView;
import com.app.sefomart.Presentation.ui.listeners.EndlessRecyclerOnScrollListener;
import com.app.sefomart.Presentation.ui.listeners.ProductClickListener;
import com.app.sefomart.R;
import com.app.sefomart.Threading.MainThreadImpl;
import com.app.sefomart.Utils.RecyclerViewMargin;
import com.app.sefomart.domain.executor.impl.ThreadExecutor;

import java.util.ArrayList;
import java.util.List;

public class OffersFragment extends Fragment implements ProductClickListener,OfferView {

    private View v;
    private List<Product> mOfferProducts = new ArrayList<>();

    private FlashDeal mFlashDeal = null;
    private OfferPresenter mOfferPresenter;
    private ProductListingAdapter adapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private TextView products_empty_text;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_offers, null);

        String title = "Flash Deals";

        adapter = new ProductListingAdapter(getContext(), mOfferProducts, this);
        recyclerView = v.findViewById(R.id.product_list);
        progressBar = v.findViewById(R.id.item_progress_bar);
        products_empty_text = v.findViewById(R.id.products_empty_text);

        GridLayoutManager horizontalLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(horizontalLayoutManager);
        RecyclerViewMargin decoration = new RecyclerViewMargin(convertDpToPx(getContext(),10), 2);
        recyclerView.addItemDecoration(decoration);
        recyclerView.setAdapter(adapter);
//        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
//            @Override
//            public void onLoadMore() {
//                addDataToList(productListingResponse);
//            }
//        });
        progressBar.setVisibility(View.VISIBLE);

        mOfferPresenter = new OfferPresenter(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this);
        mOfferPresenter.getOffers();

        return v;

    }


    @Override
    public void setOffersItems(FlashDeal flashDeal) {
        mOfferProducts.addAll(flashDeal.getProducts().getData());
        this.mFlashDeal = flashDeal;
        progressBar.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();

        if (mOfferProducts.size() <= 0){
            products_empty_text.setVisibility(View.VISIBLE);
        }
    }

    public int convertDpToPx(Context context, float dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }

    @Override
    public void onProductItemClick(Product product) {
        Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
        intent.putExtra("product_name", product.getName());
        intent.putExtra("link", product.getLinks().getDetails());
        intent.putExtra("top_selling", product.getLinks().getRelated());
        startActivity(intent);
    }
}
