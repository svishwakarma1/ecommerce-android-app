package com.app.sefomart.Presentation.presenters;

import com.app.sefomart.domain.executor.Executor;
import com.app.sefomart.domain.executor.MainThread;

public abstract class AbstractPresenter {
    protected Executor mExecutor;
    protected MainThread mMainThread;

    public AbstractPresenter(Executor executor, MainThread mainThread) {
        mExecutor = executor;
        mMainThread = mainThread;
    }
}
