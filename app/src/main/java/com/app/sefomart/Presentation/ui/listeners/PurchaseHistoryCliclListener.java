package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Models.PurchaseHistory;

public interface PurchaseHistoryCliclListener {
    void onPurchaseHistoryItemClick(PurchaseHistory purchaseHistory);
}
