package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Models.Category;

public interface AllCategoryClickListener {
    void onCategoryClick(Category category);
}
