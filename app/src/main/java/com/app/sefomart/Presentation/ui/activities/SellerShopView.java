package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Models.Product;
import com.app.sefomart.Models.Shop;

import java.util.List;

public interface SellerShopView {
    void onShopDetailsLoaded(Shop shop);
    void setFeaturedProducts(List<Product> products);
    void setTopSellingProducts(List<Product> products);
    void setNewProducts(List<Product> products);
}
