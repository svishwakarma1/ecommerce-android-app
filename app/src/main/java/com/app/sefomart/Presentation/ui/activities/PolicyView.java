package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Models.PolicyContent;

public interface PolicyView {
    void onPolicyContentLoaded(PolicyContent policyContent);
}
