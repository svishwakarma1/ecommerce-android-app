package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Network.response.RegistrationResponse;

public interface RegisterView {
     void setRegistrationResponse(RegistrationResponse registrationResponse);
}
