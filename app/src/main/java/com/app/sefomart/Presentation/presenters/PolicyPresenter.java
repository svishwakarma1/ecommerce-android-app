package com.app.sefomart.Presentation.presenters;

import com.app.sefomart.Models.PolicyContent;
import com.app.sefomart.Presentation.ui.activities.PolicyView;
import com.app.sefomart.domain.executor.Executor;
import com.app.sefomart.domain.executor.MainThread;
import com.app.sefomart.domain.interactors.PolicyInteractor;
import com.app.sefomart.domain.interactors.impl.PolicyInteractorImpl;

public class PolicyPresenter extends AbstractPresenter implements PolicyInteractor.CallBack {
    private PolicyView policyView;
    private int type = 0;

    public PolicyPresenter(Executor executor, MainThread mainThread, PolicyView policyView) {
        super(executor, mainThread);
        this.policyView = policyView;
    }

    public void getPolicy(String url){
        new PolicyInteractorImpl(mExecutor, mMainThread, this, url).execute();
    }

    @Override
    public void onPolicyLoaded(PolicyContent policyContent) {
        if (policyView != null){
            policyView.onPolicyContentLoaded(policyContent);
        }
    }

    @Override
    public void onPolicyLoadError() {

    }
}
