package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Models.SearchProduct;

public interface SearchProductClickListener {
    void onProductItemClick(SearchProduct product);
}
