package com.app.sefomart.Presentation.ui.fragments;

import com.app.sefomart.Models.AuctionProduct;
import com.app.sefomart.Models.Banner;
import com.app.sefomart.Models.Brand;
import com.app.sefomart.Models.Category;
import com.app.sefomart.Models.FlashDeal;
import com.app.sefomart.Models.Product;
import com.app.sefomart.Models.SliderImage;
import com.app.sefomart.Network.response.AppSettingsResponse;
import com.app.sefomart.Network.response.AuctionBidResponse;

import java.util.List;

public interface HomeView {
    void onAppSettingsLoaded(AppSettingsResponse appSettingsResponse);

    void setSliderImages(List<SliderImage> sliderImages);

    void setHomeCategories(List<Category> categories);

    void setTodaysDeal(List<Product> products);

    void setFlashDeal(FlashDeal flashDeal);

    void setBanners(List<Banner> banners);

    void setBestSelling(List<Product> products);

    void setFeaturedProducts(List<Product> products);

    void setTopCategories(List<Category> categories);

    void setPopularBrands(List<Brand> brands);

    void setAuctionProducts(List<AuctionProduct> auctionProducts);

    void onAuctionBidSubmitted(AuctionBidResponse auctionBidResponse);
}