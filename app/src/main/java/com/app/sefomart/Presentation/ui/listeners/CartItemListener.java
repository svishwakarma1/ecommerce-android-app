package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Models.CartModel;

public interface CartItemListener {
    void onCartRemove(CartModel cartModel);
    void onQuantityUpdate(int quantity, CartModel cartModel);
}
