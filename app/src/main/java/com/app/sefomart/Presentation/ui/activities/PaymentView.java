package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Network.response.CouponResponse;
import com.app.sefomart.Network.response.OrderResponse;

public interface PaymentView {
    void onCouponApplied(CouponResponse couponResponse);
    void onOrderSubmitted(OrderResponse orderResponse);
}
