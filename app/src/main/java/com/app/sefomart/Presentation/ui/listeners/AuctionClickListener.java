package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Models.AuctionProduct;

public interface AuctionClickListener {
    void onAuctionItemClick(AuctionProduct auctionProduct);
}
