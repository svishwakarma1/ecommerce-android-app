package com.app.sefomart.Presentation.presenters;

import com.app.sefomart.Models.FlashDeal;
import com.app.sefomart.Presentation.ui.fragments.OfferView;
import com.app.sefomart.domain.executor.Executor;
import com.app.sefomart.domain.executor.MainThread;
import com.app.sefomart.domain.interactors.FlashDealInteractor;
import com.app.sefomart.domain.interactors.impl.FlashDealInteractorImpl;

public class OfferPresenter extends AbstractPresenter implements FlashDealInteractor.CallBack {

    private OfferView offerView;

    public OfferPresenter(Executor executor, MainThread mainThread, OfferView mOfferView) {
        super(executor, mainThread);
        this.offerView = mOfferView;
    }

    public void getOffers() {
        new FlashDealInteractorImpl(mExecutor, mMainThread, this).execute();
    }

    @Override
    public void onFlashDealProductDownloaded(FlashDeal flashDeal) {
        offerView.setOffersItems(flashDeal);
    }

    @Override
    public void onFlashDealProductDownloadError() {

    }
}
