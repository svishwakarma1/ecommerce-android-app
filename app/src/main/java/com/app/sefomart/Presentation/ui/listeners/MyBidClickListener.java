package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Models.UserBid;

public interface MyBidClickListener {
    void onMyBidItemClicked(UserBid userBid);
}
