package com.app.sefomart.Presentation.ui.fragments;

import com.app.sefomart.Models.Category;

import java.util.List;

public interface CategoryView {

    void setAllCategories(List<Category> categories);
}
