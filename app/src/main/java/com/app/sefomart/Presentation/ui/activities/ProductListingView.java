package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Network.response.ProductListingResponse;

public interface ProductListingView {
    void setProducts(ProductListingResponse productListingResponse);
}
