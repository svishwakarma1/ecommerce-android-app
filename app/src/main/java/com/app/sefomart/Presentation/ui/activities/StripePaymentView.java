package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Network.response.StripeClientSecretResponse;

public interface StripePaymentView {
    void onClientSecretReceived(StripeClientSecretResponse stripeClientSecretResponse);
}
