package com.app.sefomart.Presentation.ui.fragments;

import com.app.sefomart.Network.response.LogoutResponse;

public interface AccountView {
    void showLogoutMessage(LogoutResponse logoutResponse);
}
