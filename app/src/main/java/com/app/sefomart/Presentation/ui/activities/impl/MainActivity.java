package com.app.sefomart.Presentation.ui.activities.impl;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.app.sefomart.BuildConfig;
import com.app.sefomart.Network.response.AppSettingsResponse;
import com.app.sefomart.Presentation.ui.fragments.impl.AccountFragment;
import com.app.sefomart.Presentation.ui.fragments.impl.CartFragment;
import com.app.sefomart.Presentation.ui.fragments.impl.CategoriesFragment;
import com.app.sefomart.Presentation.ui.fragments.impl.HomeFragment;
import com.app.sefomart.Presentation.ui.fragments.impl.OffersFragment;
import com.app.sefomart.Presentation.ui.fragments.impl.ProductSearchFragment;
import com.app.sefomart.R;
import com.app.sefomart.Threading.MainThreadImpl;
import com.app.sefomart.Utils.CustomToast;
import com.app.sefomart.Utils.UserPrefs;
import com.app.sefomart.domain.executor.impl.ThreadExecutor;
import com.app.sefomart.domain.interactors.AppSettingsInteractor;
import com.app.sefomart.domain.interactors.impl.AppSettingsInteractorImpl;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;

import q.rorbin.badgeview.QBadgeView;

public class MainActivity extends AppCompatActivity implements AppSettingsInteractor.CallBack {

    final Fragment homeFragment = new HomeFragment();
    final Fragment categoriesFragment = new CategoriesFragment();
    private Fragment cartFragment = new CartFragment();
    private Fragment accountFragment = new AccountFragment();
    private Fragment searchFragment = new ProductSearchFragment();
    private Fragment offersFragment = new OffersFragment();
    final FragmentManager fm = getSupportFragmentManager();
    private Fragment active = homeFragment;
    public static BottomNavigationView navView;
//    private ImageButton cart, search;
//    private TextView title;

    private DrawerLayout mDrawer;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationView mDrawerNav;


    // bottom nav clicks
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_home:
//                    title.setText(R.string.app_name);
                loadFragment(homeFragment);
                break;
//            case R.id.navigation_categories:
////                    title.setText("Categories");
//                loadFragment(categoriesFragment);
//                break;
//            case R.id.navigation_search:
////                    title.setText("Search");
//                loadFragment(searchFragment);
//                break;

            case R.id.navigation_earn_money:
                break;
            case R.id.navigation_offers:
                loadFragment(offersFragment);
                break;
            case R.id.navigation_cart:
//                    title.setText("Shopping Cart");
                loadFragment(cartFragment);
                break;
            case R.id.navigation_account:
//                    title.setText("My Account");
                loadFragment(accountFragment);
                break;
        }
                return true;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mToolbar.setTitle("Sefomart");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawer = findViewById(R.id.layout_container);
        mDrawerNav = findViewById(R.id.drawer_nav_view);
        setupDrawerContent(mDrawerNav);
        disableNavigationViewScrollbars(mDrawerNav);

        mDrawerToggle = setupDrawerToggle();
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerToggle.syncState();
        mDrawer.addDrawerListener(mDrawerToggle);

//        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setDisplayShowCustomEnabled(true);
//        getSupportActionBar().setCustomView(R.layout.custom_action_bar_layout);
//        getSupportActionBar().setElevation(0);

//        View view = getSupportActionBar().getCustomView();

//        cart = view.findViewById(R.id.action_bar_cart);
//        search = view.findViewById(R.id.action_bar_search);
//        title = view.findViewById(R.id.nav_title);


//        title.setText(R.string.app_name);

//        cart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                navView.setSelectedItemId(R.id.navigation_cart);
//                loadFragment(cartFragment);
//            }
//        });
//
//        search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                navView.setSelectedItemId(R.id.navigation_search);
//                loadFragment(searchFragment);
//            }
//        });

        navView = findViewById(R.id.nav_view);

        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) navView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(3); // number of menu from left
        new QBadgeView(this).bindTarget(v).setBadgeText(String.valueOf(0)).setShowShadow(false);

        fm.beginTransaction().add(R.id.fragment_container, categoriesFragment, "categories").hide(categoriesFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, searchFragment, "search").hide(searchFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, cartFragment, "cart").hide(cartFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, accountFragment, "account").hide(accountFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, offersFragment, "offers").hide(offersFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, homeFragment, "home").commit();


        loadFragment(homeFragment);
    }

    private void openPolicyActivity(String title, String url) {
        Intent it = new Intent(MainActivity.this, PolicyViewActivity.class);
        it.putExtra("title", title);
        it.putExtra("url", url);
        startActivity(it);
    }

    private void openDialer() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:+919540680001"));
        startActivity(intent);
    }

   // handles drawer toggle
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_toolbar_menu, menu);
        return true;
    }

    // tool bar clicks handler
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerToggle.onOptionsItemSelected(item)) return true;
            break;
            case R.id.tb_search:
                loadFragment(searchFragment);
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(menuItem -> {
                    selectDrawerItem(menuItem);
                    return true;
        });
    }

    // drawer nav clicks handler
    public void selectDrawerItem(MenuItem menuItem) {

        Fragment fragment = null;
        switch(menuItem.getItemId()) {

            // customer care
            case R.id.menu_g3_i1:
                openDialer();
            break;

            // privacy policies
            case R.id.menu_g3_i3 :
                openPolicyActivity("Privacy Policies", "policies/support");
            break;

            // seller policies
            case R.id.menu_g3_i5:
                openPolicyActivity("Seller Policies", "policies/seller");
            break;

            // Third party policies
            case R.id.menu_g3_i6:
                openPolicyActivity("Third Party Policies", "policies/return");
            break;

            // Flash sale
            case R.id.menu_g1_i3:
                if(navView.getSelectedItemId() != R.id.navigation_offers)
                    navView.setSelectedItemId(R.id.navigation_offers);
            break;

            default:
        }
        // loadFragment
        mDrawerNav.setCheckedItem(menuItem);
        setTitle(menuItem.getTitle());
        mDrawer.closeDrawers();
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {

            if (fragment != homeFragment){
//                cart.setVisibility(View.GONE);
//                search.setVisibility(View.GONE);
            } else {
//                cart.setVisibility(View.VISIBLE);
//                search.setVisibility(View.VISIBLE);
            }

            if(fragment == cartFragment){
                cartFragment = new CartFragment();
                fm.beginTransaction().remove(fragment).commitAllowingStateLoss();
                fm.beginTransaction().add(R.id.fragment_container, cartFragment, "cart").hide(cartFragment).commitAllowingStateLoss();
                fm.beginTransaction().hide(active).show(cartFragment).commitAllowingStateLoss();
                active = cartFragment;
            } else if (fragment == accountFragment){
                accountFragment = new AccountFragment();
                fm.beginTransaction().remove(fragment).commitAllowingStateLoss();
                fm.beginTransaction().add(R.id.fragment_container, accountFragment, "account").hide(accountFragment).commitAllowingStateLoss();
                fm.beginTransaction().hide(active).show(accountFragment).commitAllowingStateLoss();
                active = accountFragment;
            } else if(fragment == offersFragment) {
                offersFragment = new OffersFragment();
                fm.beginTransaction().remove(fragment).commitAllowingStateLoss();
                fm.beginTransaction().add(R.id.fragment_container, offersFragment, "offers").hide(offersFragment).commitAllowingStateLoss();
                fm.beginTransaction().hide(active).show(offersFragment).commitAllowingStateLoss();
                active = offersFragment;
            } else {
                fm.beginTransaction().hide(active).show(fragment).commit();
                active = fragment;
            }

        }
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getIntent().getExtras() != null){
            String message = getIntent().getStringExtra("message");
            String position = getIntent().getStringExtra("position");

            CustomToast.showToast(this, message, R.color.colorSuccess);
            getIntent().removeExtra("message");
            getIntent().removeExtra("position");

            if(position.equals("cart")){
                loadFragment(cartFragment);
                navView.setSelectedItemId(R.id.navigation_cart);
            } else if (position.equals("account")){
                loadFragment(accountFragment);
                navView.setSelectedItemId(R.id.navigation_account);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else if (active == homeFragment){
            super.onBackPressed();
        } else {
            loadFragment(homeFragment);
            navView.setSelectedItemId(R.id.navigation_home);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            new AppSettingsInteractorImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this).execute();
        } else if (resultCode == Activity.RESULT_CANCELED) {

        }
    }

    @Override
    public void onAppSettingsLoaded(AppSettingsResponse appSettingsResponse) {
        UserPrefs userPrefs = new UserPrefs(this);
        userPrefs.setAppSettingsPreferenceObject(appSettingsResponse, "app_settings_response");
        accountFragment = new AccountFragment();
        fm.beginTransaction().remove(accountFragment).commitAllowingStateLoss();
        fm.beginTransaction().add(R.id.fragment_container, accountFragment, "account").hide(accountFragment).commitAllowingStateLoss();
        fm.beginTransaction().hide(active).show(accountFragment).commitAllowingStateLoss();
        active = accountFragment;
    }

    @Override
    public void onAppSettingsLoadedError() {

    }
}