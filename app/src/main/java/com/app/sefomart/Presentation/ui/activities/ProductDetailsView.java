package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Models.Product;
import com.app.sefomart.Models.ProductDetails;
import com.app.sefomart.Network.response.AddToCartResponse;
import com.app.sefomart.Network.response.AddToWishlistResponse;
import com.app.sefomart.Network.response.CheckWishlistResponse;
import com.app.sefomart.Network.response.RemoveWishlistResponse;

import java.util.List;

public interface ProductDetailsView {
    void setProductDetails(ProductDetails productDetails);
    void setRelatedProducts(List<Product> relatedProducts);
    void setTopSellingProducts(List<Product> topSellingProducts);
    void setAddToCartMessage(AddToCartResponse addToCartResponse);
    void setAddToWishlistMessage(AddToWishlistResponse addToWishlistMessage);
    void onCheckWishlist(CheckWishlistResponse checkWishlistResponse);
    void onRemoveFromWishlist(RemoveWishlistResponse removeWishlistResponse);
}
