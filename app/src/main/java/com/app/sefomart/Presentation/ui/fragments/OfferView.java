package com.app.sefomart.Presentation.ui.fragments;

import com.app.sefomart.Models.FlashDeal;
import com.app.sefomart.Network.response.FlashDealResponse;

public interface OfferView {

    void setOffersItems(FlashDeal flashDeal);

}
