package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Models.UserBid;
import com.app.sefomart.Network.response.AuctionBidResponse;

import java.util.List;

public interface MybidView {
    void setUserBids(List<UserBid> userBids);
    void onAuctionBidSubmitted(AuctionBidResponse auctionBidResponse);
}
