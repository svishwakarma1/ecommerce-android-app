package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Models.WishlistModel;

import java.util.List;

public interface WishlistView {
    void setWishlistProducts(List<WishlistModel> wishlistModels);
}
