package com.app.sefomart.Presentation.ui.activities.impl;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.sefomart.Models.Category;
import com.app.sefomart.Models.SubCategory;
import com.app.sefomart.Models.SubSubCategory;
import com.app.sefomart.Presentation.presenters.SubSubCategoryPresenter;
import com.app.sefomart.Presentation.ui.activities.SubSubCategoryView;
import com.app.sefomart.Presentation.ui.adapters.SubCategoryAdapter;
import com.app.sefomart.Presentation.ui.listeners.SubSubCategoryClickListener;
import com.app.sefomart.R;
import com.app.sefomart.Threading.MainThreadImpl;
import com.app.sefomart.domain.executor.impl.ThreadExecutor;

import java.util.List;

public class SubSubCategoryActivity extends BaseActivity implements SubSubCategoryView, SubSubCategoryClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_sub_category);

        Category category = (Category) getIntent().getSerializableExtra("category");

        initializeActionBar();
        setTitle(category.getName());

        SubSubCategoryPresenter subSubCategoryPresenter = new SubSubCategoryPresenter(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this);
        subSubCategoryPresenter.getSubSubCategories(category.getLinks().getSubCategories());
    }

    @Override
    public void setSubSubCategories(List<SubCategory> subCategories) {
        RecyclerView recyclerView = findViewById(R.id.subcategory_list);
        LinearLayoutManager linearLayoutManager
                = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        SubCategoryAdapter adapter = new SubCategoryAdapter(this, subCategories);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onSubSubCategoryClick(SubSubCategory subSubCategory) {
        Intent intent = new Intent(this, ProductListingActivity.class);
        intent.putExtra("title", subSubCategory.getName());
        intent.putExtra("url", subSubCategory.getLinks().getProducts());
        startActivity(intent);
    }
}
