package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Models.Review;

import java.util.List;

public interface ProductReviewView {
    void onReviewsLoaded(List<Review> reviews);
}
