package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Network.response.AddToCartResponse;
import com.app.sefomart.Network.response.VariantResponse;

public interface BuyingOptionView {
    void setVariantprice(VariantResponse variantResponse);
    void setAddToCartMessage(AddToCartResponse addToCartResponse);
}
