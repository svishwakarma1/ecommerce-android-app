package com.app.sefomart.Presentation.ui.fragments;

import com.app.sefomart.Network.response.ProductSearchResponse;

public interface ProductSearchView {
    void setSearchedProduct(ProductSearchResponse productSearchResponse);
}
