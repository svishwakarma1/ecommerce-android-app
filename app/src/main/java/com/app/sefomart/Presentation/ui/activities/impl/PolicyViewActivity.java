package com.app.sefomart.Presentation.ui.activities.impl;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.app.sefomart.Models.PolicyContent;
import com.app.sefomart.Presentation.presenters.PolicyPresenter;
import com.app.sefomart.Presentation.ui.activities.PolicyView;
import com.app.sefomart.R;
import com.app.sefomart.Threading.MainThreadImpl;
import com.app.sefomart.domain.executor.impl.ThreadExecutor;

import org.json.JSONException;
import org.json.JSONObject;

public class PolicyViewActivity extends BaseActivity implements PolicyView {
    private String title, url;
    private TextView policy_content;
    private WebView policy_webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        title = getIntent().getStringExtra("title");
        url = getIntent().getStringExtra("url");

        initializeActionBar();
        setTitle(title);

        policy_content = findViewById(R.id.policy_content);
        policy_webview = findViewById(R.id.policy_webview);


        new PolicyPresenter(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this).getPolicy(url);
    }

    @Override
    public void onPolicyContentLoaded(PolicyContent policyContent) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            policy_content.setText(Html.fromHtml(policyContent.getContent(), Html.FROM_HTML_MODE_COMPACT));
//        } else {
//            policy_content.setText(Html.fromHtml(policyContent.getContent()));
//        }
        policy_webview.getSettings().setJavaScriptEnabled(true);
        policy_webview.getSettings().setUseWideViewPort(true);
        policy_webview.getSettings().setLoadWithOverviewMode(true);
        policy_webview.getSettings().setSupportZoom(true);
        policy_webview.getSettings().setBuiltInZoomControls(true);
        policy_webview.getSettings().setDisplayZoomControls(false);
        policy_webview.loadDataWithBaseURL(null,policyContent.getContent(), "text/html", "UTF-8",null);

    }

}
