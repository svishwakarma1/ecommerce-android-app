package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Models.User;
import com.app.sefomart.Network.response.ProfileInfoUpdateResponse;
import com.app.sefomart.Network.response.ShippingInfoUpdateResponse;

public interface AccountInfoView {
    void onProfileInfoUpdated(ProfileInfoUpdateResponse profileInfoUpdateResponse);
    void onShippingInfoUpdated(ShippingInfoUpdateResponse shippingInfoUpdateResponse);
    void setUpdatedUserInfo(User user);
}
