package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Models.Category;

public interface CategoryClickListener {
    void onCategoryItemClick(Category category);
}
