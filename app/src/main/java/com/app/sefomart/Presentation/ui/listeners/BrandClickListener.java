package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Models.Brand;

public interface BrandClickListener {
    void onBrandItemClick(Brand brand);
}
