package com.app.sefomart.Presentation.ui.fragments;

import com.app.sefomart.Models.CartModel;
import com.app.sefomart.Network.response.CartQuantityUpdateResponse;
import com.app.sefomart.Network.response.RemoveCartResponse;

import java.util.List;

public interface CartView {
    void setCartItems(List<CartModel> cartItems);
    void showRemoveCartMessage(RemoveCartResponse removeCartResponse);
    void showCartQuantityUpdateMessage(CartQuantityUpdateResponse cartQuantityUpdateResponse);
}
