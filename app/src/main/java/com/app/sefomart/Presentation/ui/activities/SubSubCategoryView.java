package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Models.SubCategory;

import java.util.List;

public interface SubSubCategoryView {
    void setSubSubCategories(List<SubCategory> subSubCategories);
}
