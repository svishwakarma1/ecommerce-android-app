package com.app.sefomart.Presentation.ui.listeners;

import com.app.sefomart.Models.Product;

public interface ProductClickListener {
    void onProductItemClick(Product product);
}
