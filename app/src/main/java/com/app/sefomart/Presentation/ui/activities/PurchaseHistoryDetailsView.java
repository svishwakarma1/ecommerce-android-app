package com.app.sefomart.Presentation.ui.activities;

import com.app.sefomart.Models.OrderDetail;

import java.util.List;

public interface PurchaseHistoryDetailsView {
    void onOrderDetailsLoaded(List<OrderDetail> orderDetailList);
}
